package com.jorgegarcia.firstexam.BusquedaCliente;

import java.util.ArrayList;

public class PersonaService{
    private String aux;
    private ArrayList<PersonaEntity> personas;
    private PersonaEntity persona;
    public PersonaService(){
      aux = "";
      personas = new ArrayList<PersonaEntity>();
      initPersonas();
    }

    private void initPersonas(){
      personas.add( new PersonaEntity((personas.size() + 1), "Jorge", "Garcia", "jorge.garcia"));
      personas.add( new PersonaEntity((personas.size() + 1), "Diego", "Maradona", "diego.maradona"));
      personas.add( new PersonaEntity((personas.size() + 1), "Roman", "Riquelme", "Roman123"));
      personas.add( new PersonaEntity((personas.size() + 1), "Carlos", "Tevez", "apache"));
      personas.add( new PersonaEntity((personas.size() + 1), "Lautaro", "Martines", "Lautarito10"));
    }

    public String showPersonas(){
      aux += "Id\tNombre\t\t Apellido\t\t Nombre Usuario\n---\t-----\t\t -------\t\t -----------\n";
      for( PersonaEntity person: personas){
        aux += person.toString() + "\n";
      }
      return aux;
    }

    public void addPersona(String nombre, String apellido, String nombreUsuario){
      persona = new PersonaEntity( (personas.size() + 1), nombre, apellido, nombreUsuario);
      personas.add(persona);
    }

    public PersonaEntity findByName(String name){
      int i = getIndiceName(name);
      if(i == -1)
        return null;
      return personas.get(i);
      
    }

    public PersonaEntity findByLastName(String lastName){
      int i = getIndiceLastName(lastName);
      if(i == -1)
        return null;
      return personas.get(i);
    }

    private int getIndiceName(String lastName){
      int i = 0;
      for(PersonaEntity person: personas){
        if( person.getName().equals(lastName))
          return i;
        i++;
      }
      return -1;
    }

    private int getIndiceLastName(String lastName){
      int i = 0;
      for(PersonaEntity person: personas){
        if( person.getApellido().equals(lastName))
          return i;
        i++;
      }
      return -1;
    }
}