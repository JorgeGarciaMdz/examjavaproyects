package com.jorgegarcia.firstexam.BusquedaCliente;

public class PersonaEntity{
  private int id;
  private String nombre;
  private String apellido;
  private String nombreUsuario;

  public PersonaEntity(int id, String nombre, String apellido, String nombreUsuario){
    this.id = id;
    this.nombre = nombre;
    this.apellido = apellido;
    this.nombreUsuario = nombreUsuario;
  }

  public String getName(){
    return this.nombre;
  }

  public String getApellido(){
    return this.apellido;
  }

  public String toString(){
    return this.id + "\t" + this.nombre + "\t\t" + this.apellido + "\t\t" + this.nombreUsuario;
  }
}