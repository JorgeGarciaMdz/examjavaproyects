package com.jorgegarcia.firstexam.BusquedaCliente;
import java.util.Scanner;
import java.util.InputMismatchException;

public class PersonaController{
  private PersonaService personaService;
  private PersonaEntity persona_aux;
  private String menu, head;
  private Scanner scanner;
  private String input_string;
  private int opcion;
  private String name_aux, last_aux, rename_aux;
  public PersonaController(){
    personaService = new PersonaService();
    head = "\n\t Bienvenidos a Busqueda de Personas \n\t ---------------------------------- \n";
    menu ="\t** Mostrar todas las personas \t\t1\n\t** Agregar Persona \t\t\t2\n" +
          "\t** Buscar por Nombre \t\t\t3\n\t** Buscar por Apellido \t\t\t4\n" +
          "\n\t\t **** Salir ****  \t\t0";
    opcion = 0;
  }

  public void showMenu(){
    System.out.println( head + menu );
    while( true ){
      opcion = getNumber();
      if( opcion == 0 ){
        System.out.println("\t **** Good Bye!! ****");
        return;
      }
      else if( opcion == -1 ){
        System.out.println("\t** Por favor, ingrese un numero acorde al menu de opcions **");
        System.out.println(menu);
        continue;
      } else if ( opcion > 0 && opcion <= 5){
        switch (opcion) {
          case 1:
            {
              System.out.println("\n\tOpcion " + opcion + ", Mostrar todas las personas");
              System.out.println(personaService.showPersonas());
              System.out.println("\n" + menu);
            }
            break;
          case 2:
            {
              System.out.println("\n\tOpcion " + opcion + ", Agregar Persona.");
              dataInput();
              personaService.addPersona(name_aux, last_aux, rename_aux);
              System.out.println(personaService.showPersonas());
              System.out.println("\n" + menu);
            }
            break;
          case 3:
            {
              System.out.println("\nOpcion "+ opcion + ", Buscar persona por Nombre");
              persona_aux = personaService.findByName(stringInput("nombre: "));
              if(persona_aux == null)
                System.out.println("\n*** No se han encontrado resultado con " + input_string + ". ***");
              else{
                System.out.println(persona_aux.toString());
              }
              System.out.println("\n" + menu);

            }
            break;
          case 4:
            {
              System.out.println("\nOpcion "+ opcion + ", Buscar persona por Apellido");
              persona_aux = personaService.findByLastName(stringInput("apellido: "));
              if(persona_aux == null)
                System.out.println("\n*** No se han encontrado resultado con " + input_string + ". ***");
              else{
                System.out.println(persona_aux.toString());
              }
              System.out.println("\n" + menu);
            }
            break;
        
          default:
            continue;
        }
      }

    }
  }

  private void dataInput(){
    do{
      System.out.println("Por favor, ingrese nombre: ");
      name_aux = getString();
    } while(name_aux.length() <= 3);

    do{
      System.out.println("Por favor, ingrese apellido: ");
      last_aux = getString();
    } while( last_aux.length() <= 3);

    do{
      System.out.println("Por favor ingrese nombre de usuario: ");
      rename_aux = getString();
    } while(rename_aux.length() <= 3);
  }

  private String stringInput(String type){
    do{
      System.out.println("Por favor, ingrese " + type);
      name_aux = getString();
    } while(name_aux.length() <= 3);
    return name_aux;
  }

  private int getNumber(){
    scanner = new Scanner(System.in);
    int number;
    try{
      number = scanner.nextInt();
      return number;
    } catch(InputMismatchException e){
      System.out.println("\nPlease, input a number\n");
      return -1;
    }
  }

  private String getString(){
    scanner = new Scanner(System.in);
    try{
      input_string = scanner.nextLine();
      if( (int)(input_string.length()) < 3){
        System.out.println("\n Please, length string up to  three characters");
        return "";
      }
      return input_string;
    } catch(InputMismatchException e){
      System.out.println("\nPlease, input a String\n");
      return "";
    }
  }
}