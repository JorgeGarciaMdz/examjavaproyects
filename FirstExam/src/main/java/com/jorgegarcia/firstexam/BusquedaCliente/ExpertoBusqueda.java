/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jorgegarcia.firstexam.BusquedaCliente;
import com.jorgegarcia.firstexam.Experto;

/**
 *
 * @author jorge
 */
public class ExpertoBusqueda implements Experto {
    private PersonaController personaController;
    public ExpertoBusqueda(){
        System.out.println("--> In Experto Busqueda");
        personaController = new PersonaController();
    }
    public void start(){
        System.out.println("in experto busqueda");
        personaController.showMenu();
    }
}
