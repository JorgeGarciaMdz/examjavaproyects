package com.jorgegarcia.firstexam.Agenda.service;

import java.util.ArrayList;
import java.util.Calendar;
import com.jorgegarcia.firstexam.Agenda.entity.Agenda;
import com.jorgegarcia.firstexam.Agenda.entity.Doctor;

public class AgendaService {

  private Agenda agenda;
  private DiaService diaService;
  private ArrayList<Agenda> array_agenda;
  private Calendar calendar;
  private DoctorService doctorService;
  private String MES[] = { "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre",
      "Octubre", "Noviembre", "Diciembre" };
  private String DIA[] = { "Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado" };
  private int dia_actual, ultimo_dia;

  public AgendaService() {
    calendar = Calendar.getInstance();
    array_agenda = new ArrayList<Agenda>();
    this.diaService = new DiaService();
    this.doctorService = new DoctorService();
    init();

  }

  public void createAgenda(Doctor doctor) {
    this.agenda = new Agenda( array_agenda.size() + 1, MES[calendar.get(Calendar.MONTH)]);
    dia_actual = calendar.get(Calendar.DAY_OF_MONTH);
    ultimo_dia = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
    for (int i = dia_actual; i <= ultimo_dia; i++) {
      // System.out.println(i + " " + DIA[(i + 1) % 7]);
      this.agenda.addDias(diaService.createDia(i, DIA[(i + 1) % 7]));
    }
    this.agenda.setDoctor(doctor);
    array_agenda.add(this.agenda);
    System.out.println("\t-------------------------------------\n\tSe ha generado agenda para el doctor: \n"+
                doctor.toString() + "\n\tCon Agenda: \n" + this.agenda.toString() + 
                "\n\tSe ha generado agenda para el doctor: \n" + doctor.toString() );
  }

  public void setDoctor(int id, Doctor doctor) {
    for (Agenda agenda : array_agenda)
      if (agenda.getId() == id)
        agenda.setDoctor(doctor);
  }

  public Agenda findAgendaByMatriculaDoctor(int matricula) {
    for (Agenda agenda : array_agenda) {
      if (agenda.getDoctor().getMatricula() == matricula)
        return agenda;
    }
    return null;
  }

  // creo una agenda
  private void init() {
    // String cadena = "";
    dia_actual = calendar.get(Calendar.DAY_OF_MONTH);
    ultimo_dia = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
    this.array_agenda.add(new Agenda(array_agenda.size() + 1, MES[calendar.get(Calendar.MONTH)]));

    this.array_agenda.get(0).setDoctor(doctorService.findById(1));

    for (int i = dia_actual; i <= ultimo_dia; i++) {
      // System.out.println(i + " " + DIA[(i + 1) % 7]);
      this.array_agenda.get(0).addDias(diaService.createDia(i, DIA[(i + 1) % 7]));
    }
    // for (Dia dia : this.array_agenda.get(0).getAllDias())
    // cadena += dia.toString() + "\n";
    // cadena += this.array_agenda.get(0).getDoctor().toString();
    // // System.out.println("Dias: \n" + cadena);
  }

  // DIA[calendario.get(Calendar.DAY_OF_WEEK) - 1] obtiene dia acual en string
  // calendario.get(Calendar.DAY_OF_MONTH) obtiene el numero del dia
  // MES[calendario.get(Calendar.MONTH)] obtiene el mes en string

}