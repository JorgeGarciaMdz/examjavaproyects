package com.jorgegarcia.firstexam.Agenda.service;

import com.jorgegarcia.firstexam.Agenda.entity.Paciente;
import com.jorgegarcia.firstexam.Agenda.entity.Turno;

public class TurnoService {

  public TurnoService() {
  }

  public Turno createTurno(String motivo, int hora_inicio, int minuto_inicio, int hora_fin, int minuto_fin,
      Paciente paciente) {
    return new Turno(motivo, hora_inicio, minuto_inicio, hora_fin, minuto_fin, paciente);
  }
}