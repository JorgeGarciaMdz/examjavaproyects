package com.jorgegarcia.firstexam.Agenda.entity;

import java.util.ArrayList;

// modelar turno tarde o turno mañana del dia y hora inicio y fin del turno
public class DiaTurno {
  private char letra;
  private String nombre;
  private int hora_inicio, minuto_inicio;
  private int hora_fin, minuto_fin;
  ArrayList<Turno> array_turnos;

  public DiaTurno(char letra, String nombre, int hora_inicio, int minuto_inicio, int hora_fin, int minuto_fin) {
    this.letra = letra;
    this.nombre = nombre;
    this.hora_inicio = hora_inicio;
    this.minuto_inicio = minuto_inicio;
    this.hora_fin = hora_fin;
    this.minuto_fin = minuto_fin;
    array_turnos = new ArrayList<Turno>();
  }

  // public void addTurno(Turno turno){
  // array_turnos.add(turno);
  // }

  public void addTurno(Turno turno) {
    System.out.println(" **** DiaTurno addTurno ***\nCantidad de turnos existentes: " + array_turnos.size());
    Turno nextT, aux;
    turno.setId(array_turnos.size() + 1);
    if (array_turnos.size() == 0) {
      // System.out.println("Solo un turno:\n" + turno.toString());
      array_turnos.add(turno);
    } else if (array_turnos.size() == 1) {
      nextT = array_turnos.get(0);
      if ((nextT.getHoraInicio() * 60 + nextT.getMinutoInicio()
          + nextT.getDuracionMunitos()) < (turno.getHoraInicio() * 60 + turno.getMinutoInicio())) {
        array_turnos.add(turno);
      } else {
        array_turnos.add(0, turno);
      }
      // System.out.print("Dos turnos: \n" + array_turnos.get(0).toString() + "\n" + array_turnos.get(1));
    } else {
      // System.out.println("--> size: " + array_turnos.size());
      for(int i = array_turnos.size() - 1; i > 0; i--){ // comparo desde el ultimo elemento de la lista
        aux = array_turnos.get(i); // me traigo el ultimo elemento
        // System.out.println("turno i: " + i + "\n"+  aux.toString() + "\n turno ingreso:\n" + turno.toString());
        if( aux.getInicioMinutos() >= ( turno.getInicioMinutos() + turno.getDuracionMunitos() ) ){
          // System.out.println(( turno.getInicioMinutos() + turno.getDuracionMunitos() ) + "  " + aux.getInicioMinutos()); 
          aux = array_turnos.get(i-1);
          if( (aux.getInicioMinutos() + aux.getDuracionMunitos()) < turno.getInicioMinutos()){
            // System.out.println( ( aux.getInicioMinutos() + aux.getDuracionMunitos()) + " " + turno.getInicioMinutos());
            array_turnos.add(i, turno);
          } else {
            if( (hora_inicio * 60 + minuto_inicio ) <= turno.getInicioMinutos() && 
                aux.getInicioMinutos() >= (turno.getInicioMinutos() + turno.getDuracionMunitos()) ){
              array_turnos.add(0, turno);
              return ;
            } else {
              System.out.println("El turno se solapa con otros: \n"+ array_turnos.get(i).toString() + "\n\t ----------------\n" + 
                  "turno a ingresar: \n" + turno.toString() + "\n\t---------------------\n" + aux.toString());
            return;
            }
          }
        }
      }
    }
  }
  

  public Turno getTurno(int i) {
    for (Turno turno : array_turnos)
      if (turno.getId() == i)
        return array_turnos.get(i);
    return null;
  }

  public ArrayList<Turno> getAllTurnos() {
    return array_turnos;
  }

  public char getLetra() {
    return this.letra;
  }

  public String getNombre() {
    return this.nombre;
  }

  public void setNombre(String nombre) {
    this.nombre = nombre;
  }

  public int getHoraInicio() {
    return this.hora_inicio;
  }

  public void setHoraInicio(int hora_inicio) {
    this.hora_inicio = hora_inicio;
  }

  public int getMinutoInicio() {
    return this.minuto_inicio;
  }

  public void setMinutoInicio(int minuto_inicio) {
    this.minuto_inicio = minuto_inicio;
  }

  public int getHoraFin() {
    return this.hora_fin;
  }

  public void setHoraFin(int hora_fin) {
    this.hora_fin = hora_fin;
  }

  public int getMinutoFin() {
    return this.minuto_fin;
  }

  public void setMinutoFin(int minuto_fin) {
    this.minuto_fin = minuto_fin;
  }

  public String getHoraMinutoInicio() {
    return this.hora_inicio + ":" + this.minuto_inicio;
  }

  public String getHoraMinutoFin() {
    return this.hora_fin + ":" + this.minuto_fin;
  }

  public String toString() {
    return "\tTurno " + this.nombre + "\tDesde: " + this.hora_inicio + ":" + this.minuto_inicio + "\tHasta: "
        + this.hora_fin + ":" + this.minuto_fin;
  }
}