package com.jorgegarcia.firstexam.Agenda.entity;

import java.util.ArrayList;

public class Agenda {
  private int id;
  private String mes;
  private ArrayList<Dia> array_dias;
  private Doctor doctor;

  public Agenda(int id, String mes) {
    this.id = id;
    this.mes = mes;
    array_dias = new ArrayList<Dia>();
  }

  public int getId() {
    return this.id;
  }

  public String getMes() {
    return this.mes;
  }

  public void addDias(Dia dia) {
    array_dias.add(dia);
  }

  public ArrayList<Dia> getAllDias() {
    return array_dias;
  }

  public Dia getDay(int day){
    for( Dia dia: array_dias)
      if( dia.getDia() == day)
        return dia;
    return null;
  }

  public Doctor getDoctor() {
    return this.doctor;
  }

  public void setDoctor(Doctor doctor) {
    this.doctor = doctor;
  }

  public String toString() {
    String days = "\tId: " + this.id + "\tMes: " + this.mes + "\n\n";
    for (Dia dia : array_dias) {
      if (dia.getDia() % 7 < 5){
        days += dia.toString() + "\n";
      } else if( dia.getDia() % 7 == 5) {
        days += "\n";
      }
    }
    return days;
  }
}