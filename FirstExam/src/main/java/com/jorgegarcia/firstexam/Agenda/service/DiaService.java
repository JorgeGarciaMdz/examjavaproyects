package com.jorgegarcia.firstexam.Agenda.service;

import com.jorgegarcia.firstexam.Agenda.entity.Dia;

public class DiaService {

  public DiaService(){
  }

  public Dia createDia(int dia, String nombre){
    return new Dia(dia, nombre);
  }
}