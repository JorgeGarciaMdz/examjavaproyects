/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jorgegarcia.firstexam.Agenda;
import com.jorgegarcia.firstexam.Experto;

/**
 *
 * @author jorge
 */
public class ExpertoAgenda implements Experto {  
    
    private AgendaLogica agendaLogica;
    
    public ExpertoAgenda(){
        agendaLogica = new AgendaLogica();
    }
    
    public void start(){
        agendaLogica.init();
    }
}
