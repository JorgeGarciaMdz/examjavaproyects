package com.jorgegarcia.firstexam.Agenda.service;

import java.util.ArrayList;

import com.jorgegarcia.firstexam.Agenda.entity.Doctor;

public class DoctorService {

  private Doctor doctor;
  private ArrayList<Doctor> array_doctor;

  public DoctorService() {
    array_doctor = new ArrayList<Doctor>();
    array_doctor.add(new Doctor(array_doctor.size() + 1, "Pedro", "Picapiedra", 12345, 35));
    array_doctor.add(new Doctor(array_doctor.size() + 1, "Pablo", "Marmol", 98007, 2202));
  }

  public void createDoctor(String nombre, String apellido, int dni, int matricula) {
    doctor = findByMatricula(matricula);
    if (doctor == null){
      doctor = new Doctor((array_doctor.size() + 1),nombre, apellido, dni, matricula);
      array_doctor.add(doctor);
      System.out.print("-----------------------------\nIngreso del doctor exitoso!:\n" +
                      doctor.toString() + "\n-----------------------------\n");
    }
    else {
      System.out.print("-----------------------------\nYa existe un doctor don matricula:" + matricula +
                      "\n" + doctor.toString() + "\n-----------------------------\n");
    }
  }

  public Doctor findById(int id) {
    for (Doctor doctor : array_doctor)
      if (doctor.getId() == id)
        return doctor;
    return null;
  }

  public Doctor findByMatricula(int matricula) {
    for (Doctor doctor : array_doctor){
      if (doctor.getMatricula() == matricula){
        return doctor;
      }
    }
    return null;
  }

  public Doctor findByDni(int dni) {
    for (Doctor doctor : array_doctor)
      if (doctor.getDni() == dni)
        return doctor;
    return null;
  }

  public void showDoctors(){
    String doctores = "\tTodos los doctores actuales:\n" + 
                      "\t---------------------------\n";
    for(Doctor doctor: array_doctor)
      doctores += doctor.toString() + "\n";
    System.out.println(doctores);
  }
}