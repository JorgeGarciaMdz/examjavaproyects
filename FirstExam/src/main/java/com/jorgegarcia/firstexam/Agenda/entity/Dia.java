package com.jorgegarcia.firstexam.Agenda.entity;

// modelar solo el nombre del dia y el numero del dia
public class Dia {
  private int dia;
  private String nombre;
  private DiaTurno mañana, tarde;

  public Dia(int dia, String nombre) {
    this.dia = dia;
    this.nombre = nombre;
    this.mañana = null;
    this.tarde = null;
  }

  public int getDia() {
    return this.dia;
  }

  public String getNombre() {
    return this.nombre;
  }

  public DiaTurno getTurno(char turno) {
    switch (turno) {
      case 'm':
        return this.mañana;
      case 't':
        return this.tarde;
      default:
        return null;
    }
  }

  public void setTurno(DiaTurno diaTurno) {
    switch (diaTurno.getLetra()) {
      case 'm':
        this.mañana = diaTurno;
        break;
      case 't':
        this.tarde = diaTurno;
        break;
      default:
        break;
    }
  }

  public String toString() {
    String turno_mañana = "";
    String turno_tarde = "";
    if (mañana == null)
      turno_mañana += "\t*** no hay turnos AM asignados para este dia";
    else {
      turno_mañana += mañana.toString() + "\n";
      for (Turno turno : mañana.getAllTurnos())
        turno_mañana += turno.toString() + "\n";
    }
    if (tarde == null)
      turno_tarde += "\t*** No hay turnos PM asignados para este dia\n";
    else {
      turno_tarde += tarde.toString() + "\n";
      for (Turno turno : tarde.getAllTurnos())
        turno_tarde += turno.toString() + "\n";
    }
    return "\t" + this.nombre + "\t\t" + this.dia + "\n"
        + "---------------------------------------------------------------\n" + turno_mañana + "\n" + turno_tarde;
  }
}