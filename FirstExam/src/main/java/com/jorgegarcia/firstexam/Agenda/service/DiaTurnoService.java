package com.jorgegarcia.firstexam.Agenda.service;


import com.jorgegarcia.firstexam.Agenda.entity.DiaTurno;

public class DiaTurnoService {


  public DiaTurnoService() {
  }

  public DiaTurno createDiaTurno(char letra, String nombre, int hora_inicio, int minuto_inicio, int hora_fin,
      int minuto_fin) {
    
    if( letra == 'm'){
      return new DiaTurno(letra, nombre, hora_inicio, minuto_inicio, hora_fin, minuto_fin); 
    } else if( letra == 't') {
      return new DiaTurno(letra, nombre, hora_inicio, minuto_inicio, hora_fin, minuto_fin);
    } else {
      return null;
    }

  }

  // public void addTurno(Turno turno) {
  //   turno.setId(array_turnos.size() + 1);
  //   if (array_turnos.size() == 0) {
  //     array_turnos.add(turno);
  //   } else if (array_turnos.size() == 1) {
  //     next = array_turnos.get(0);
  //     if ((next.getHoraInicio() * 60 + next.getMinutoInicio() + next.getDuracionMunitos()) < (turno.getHoraInicio() * 60
  //         + turno.getMinutoInicio())) {
  //       array_turnos.add(turno);
  //     } else {
  //       array_turnos.add(0, turno);
  //     }
  //   } else {
  //     index = 0;
  //     for (Turno t : array_turnos) {
  //       index++;
  //       if (index < array_turnos.size() - 1) {
  //         next = array_turnos.get(index + 1);
  //         min_t1 = (t.getHoraInicio() * 60 + t.getMinutoInicio() + t.getDuracionMunitos());
  //         min_t2 = (next.getHoraInicio() * 60 + next.getMinutoInicio());
  //         min_new = (turno.getHoraInicio() * 60 + turno.getMinutoInicio() + turno.getDuracionMunitos());
  //         lapso = min_t2 - min_t1;
  //         if (min_new > min_t1 && min_new < min_t2 && (turno.getDuracionMunitos() <= lapso)) {
  //           array_turnos.add(index + 1, turno);
  //           break;
  //         } else {
  //           array_turnos.add(turno);
  //           break;
  //         }
  //       }
  //     }
  //   }
  // }

  // public Turno getTurnoById(int id) {
  //   for (Turno turno : array_turnos)
  //     if (turno.getId() == id)
  //       return turno;
  //   return null;
  // }

  // public void showTurnos() {
  //   all_turnos = "";
  //   for (Turno turno : array_turnos) {
  //     all_turnos += "\t" + turno.toString() + "\n";
  //   }
  //   System.out.println("\t*** Todos los tunos asignados ****\n" + all_turnos);
  // }
}