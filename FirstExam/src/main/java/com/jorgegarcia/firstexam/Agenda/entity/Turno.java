package com.jorgegarcia.firstexam.Agenda.entity;

public class Turno{
  private int id;
  private String motivo;
  private int hora_inicio, minuto_inicio;
  private int hora_fin, minuto_fin;
  private Paciente paciente;

  public Turno( String motivo, int hora_inicio, int minuto_inicio,
                int hora_fin, int minuto_fin, Paciente paciente){
    this.motivo = motivo;
    this.hora_inicio = hora_inicio;
    this.minuto_inicio = minuto_inicio;
    this.hora_fin = hora_fin;
    this.minuto_fin = minuto_fin;
    this.paciente = paciente;
  }
  public int getId(){ return this.id; }
  public void setId(int id){ this.id = id; }

  public int getHoraInicio(){ return this.hora_inicio; }
  public void setHoraInicio(int hora_inicio){ this.hora_inicio = hora_inicio; }

  public int getMinutoInicio(){ return this.minuto_inicio; }
  public void setMinutoInicio(int minuto_inicio){ this.minuto_inicio = minuto_inicio; }

  public int getHoraFin(){ return this.hora_fin;}
  public void setHoraFin(int hora_fin){ this.hora_fin = hora_fin; }

  public int getMinutoFin(){ return this.minuto_fin; }
  public void setMinutoFin(int minuto_fin){ this.minuto_fin = minuto_fin; }

  public String getMotivo(){ return this.motivo; }

  public Paciente getPaciente(){ return this.paciente; }

  public String toString(){
    return "\tHora inicio: " + this.hora_inicio + ":" + this.minuto_inicio + "\tHora fin: " + 
          this.hora_fin + ":" + this.minuto_fin + "\n\tPaciente: " + paciente.toString() + "\n";
  }

  public int getDuracionMunitos(){
    return (
      (this.hora_fin * 60 + this.minuto_fin ) - (this.hora_inicio * 60 + this.minuto_inicio )
    );
  }

  public int getInicioMinutos(){
    return ( this.hora_inicio * 60 + this.minuto_inicio);
  }
}