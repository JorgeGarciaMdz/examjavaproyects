package com.jorgegarcia.firstexam.Agenda.entity;

public class Doctor extends Persona{
  private int matricula;

  public Doctor(int id, String nombre, String apellido, int dni, int matricula) {
    super(id, nombre, apellido, dni);
    this.matricula = matricula; 
  }

  public int getMatricula(){ return this.matricula; }

  public String toString(){
    return super.toString() + "\tmatricula: " + this.matricula;
  }
}