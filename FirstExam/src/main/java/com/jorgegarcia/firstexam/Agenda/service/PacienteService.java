package com.jorgegarcia.firstexam.Agenda.service;

import java.util.ArrayList;
import com.jorgegarcia.firstexam.Agenda.entity.Paciente;

public class PacienteService {

  private ArrayList<Paciente> array_pacientes;
  private Paciente paciente;

  public PacienteService() {
    this.array_pacientes = new ArrayList<Paciente>();
    array_pacientes.add(new Paciente(array_pacientes.size() + 1, "Clark", "Ken", 49633, 1001));
    array_pacientes.add(new Paciente(array_pacientes.size() + 1, "Bruno", "Dias", 1111, 10021));
    array_pacientes.add(new Paciente(array_pacientes.size() + 1, "Benjamin", "Bottom", 3569, 10045));
    array_pacientes.add(new Paciente(array_pacientes.size() + 1, "Enzo", "Perez", 68204, 10086));
  }

  public void createPaciente(String nombre, String apellido, int dni) {
    paciente = findByDni(dni);
    if ( paciente == null) {
      paciente = new Paciente(array_pacientes.size() + 1, nombre, apellido, dni, array_pacientes.get(array_pacientes.size() -1).getIdCliente() + 1);
      array_pacientes.add(paciente);
      System.out.print("-----------------------------\nIngreso de Paciente exitoso!:\n" +
                      paciente.toString() + "\n-----------------------------\n");
    } else {
      System.out.print("-----------------------------\nYa existe el Paciente don DNI:" + dni +
                      "\n" + paciente.toString() + "\n-----------------------------\n");
    }
  }

  public Paciente findById(int id) {
    for (Paciente paciente : array_pacientes)
      if (paciente.getId() == id)
        return paciente;
    return null;
  }

  public Paciente findByDni(int dni) {
    for (Paciente paciente : array_pacientes)
      if (paciente.getDni() == dni)
        return paciente;
    return null;
  }

  public void showPacientes(){
    String pacientesString = "\tTodos los pacientes actuales actuales:\n" + 
                              "\t--------------------------------------\n";
    for(Paciente paciente: array_pacientes)
      pacientesString += paciente.toString() + "\n";
    System.out.println(pacientesString);
  }
}