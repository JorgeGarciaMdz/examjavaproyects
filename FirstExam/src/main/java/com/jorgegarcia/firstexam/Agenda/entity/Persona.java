package com.jorgegarcia.firstexam.Agenda.entity;

public class Persona{
  private int id;
  private String nombre;
  private String apellido;
  private int dni;

  public Persona(int id, String nombre, String apellido, int dni){
    this.id = id;
    this.nombre = nombre;
    this.apellido = apellido;
    this.dni = dni;
  }

  public int getId(){ return this.id; }

  public String getNombre(){ return this.nombre; }

  public String getApellido(){ return this.apellido; }

  public int getDni() { return this.dni; }

  public String toString(){
    return "id: " + this.id + "\tnombre: " + this.nombre + "\tapellido: " + this.apellido +
            "\tdni: " + this.dni;
  }
}