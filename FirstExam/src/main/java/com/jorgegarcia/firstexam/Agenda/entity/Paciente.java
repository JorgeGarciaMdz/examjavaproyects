package com.jorgegarcia.firstexam.Agenda.entity;

public class Paciente extends Persona{
  private int id_cliente;
  public Paciente(int id, String nombre, String apellido, int dni, int id_cliente) {
    super(id, nombre, apellido, dni);
    this.id_cliente = id_cliente;
  }
  
  public int getIdCliente(){ return this.id_cliente; }

  public String toString(){
    return super.toString() + "\tId Paciente: " + this.id_cliente;
  }
}