package com.jorgegarcia.firstexam.Agenda;

import java.util.InputMismatchException;
import java.util.Scanner;

import com.jorgegarcia.firstexam.Agenda.entity.Agenda;
import com.jorgegarcia.firstexam.Agenda.entity.Dia;
import com.jorgegarcia.firstexam.Agenda.entity.DiaTurno;
import com.jorgegarcia.firstexam.Agenda.entity.Doctor;
import com.jorgegarcia.firstexam.Agenda.entity.Paciente;
import com.jorgegarcia.firstexam.Agenda.entity.Turno;
import com.jorgegarcia.firstexam.Agenda.service.AgendaService;
import com.jorgegarcia.firstexam.Agenda.service.DiaTurnoService;
import com.jorgegarcia.firstexam.Agenda.service.DoctorService;
import com.jorgegarcia.firstexam.Agenda.service.PacienteService;

public class AgendaLogica {

  private String input_string;
  private Scanner scanner;
  private String menu;
  private int opcion, aux, hora_inicio, minuto_inicio, hora_fin, minuto_fin;
  private DoctorService doctorService;
  private PacienteService pacienteService;
  private Agenda agenda;
  private AgendaService agendaService;
  private Doctor doctor;
  private DiaTurnoService diaTurnoService;

  public AgendaLogica() {
    this.doctorService = new DoctorService();
    this.pacienteService = new PacienteService();
    this.agendaService = new AgendaService();
    this.diaTurnoService = new DiaTurnoService();
  }

  public void init() {
    System.out.println("\t*** Bienvenido!, Agenda del Mes ***");
    showMenu();
    while (true) {
      opcion = getNumber();
      if (opcion == 0) {
        System.out.println("\t **** Good Bye!! ****");
        return;
      } else if (opcion == -1) {
        System.out.println("\t** Por favor, ingrese un numero acorde al menu de opciones **");
        showMenu();
        continue;
      } else if (opcion > 0 && opcion <= 6) {
        switch (opcion) {
          case 1: {
            doctorService.showDoctors();
            showMenu();
          }
            break;
          case 2: {
            pacienteService.showPacientes();
            showMenu();
          }
            break;
          case 3: {
            doctorService.showDoctors();
            System.out.println("\tPor favor, ingrese la matricula del doctor:");
            aux = getNumber();
            if (aux > 0) {
              System.out.println("number: " + aux);
              doctor = doctorService.findByMatricula(aux);
              if (doctor == null) {
                System.out.println("\t*** No hay Doctor con Matricula: " + aux);
              } else {
                agenda = agendaService.findAgendaByMatriculaDoctor(aux);
                if (agenda == null) {
                  System.out
                      .println("\t***Doctor sin Agenda: \n" + doctorService.findByMatricula(aux).toString() + "\n\n");
                  showMessage(
                      "\t\t---------------------\n\tPara crear agenda para ingrese 1, caso contrario ingrese 0\n"
                          + "\t\t--------------------");
                  if (getNumber() == 1)
                    agendaService.createAgenda(doctor);
                  else {
                    System.out.println("\tNo se creara la agenda para el doctor: \n" + doctor.toString());
                  }
                } else {
                  showMessage(agenda.getDoctor().getNombre());
                  System.out.println("Agenda\n" + agenda.toString());
                }
              }
            }
            doctor = null;
            agenda = null;
            showMenu();
          }
            break;
          case 4: {
            doctorService.showDoctors();
            showMessage("\tPor favor, ingrese la matricula del doctor:");
            aux = getNumber();
            if (aux > 0) {
              System.out.println("number: " + aux);
              doctor = doctorService.findByMatricula(aux);
              if (doctor == null) {
                System.out.println("\t*** No hay Doctor con Matricula: " + aux);
              } else {
                agenda = agendaService.findAgendaByMatriculaDoctor(aux);
                if (agenda == null) {
                  System.out
                      .println("\t***Doctor sin Agenda: \n" + doctorService.findByMatricula(aux).toString() + "\n\n");
                  showMessage(
                      "\t\t---------------------\n\tPara crear agenda para ingrese 1, caso contrario ingrese 0\n"
                          + "\t\t--------------------");
                  if (getNumber() == 1)
                    agendaService.createAgenda(doctor);
                  else {
                    System.out.println("\tNo se creara la agenda para el doctor: \n" + doctor.toString());
                  }
                } else {
                  gestionarTurno(agenda);
                }
              }
            }
            doctor = null;
            agenda = null;
            showMenu();

          }
            break;

          case 5: {
            createDoctor();
            showMenu();
          }
            break;
          case 6: {
            createPaciente();
            showMenu();
          }
            break;
          default:
            showMessage("Falta implementar el caso: " + opcion);
            continue;
        }
      } else {
        showMessage("\t** Por favor, ingrese un numero acorde al menu de opciones **");
        showMenu();
      }
    }

  }

  private void createPaciente() {
    String nombre, apellido;
    int dni;
    showMessage("\t---------------------------------\nBienvenido a dar de alta a un nuevo doctor en el sistema!\n");
    nombre = dataInput("Por favor, ingrese Nombre: \t\tNo puede tener menos de 3 caracteres");
    apellido = dataInput("Por favor, ingrese Apellido: \t\tNo puede tener menos de 3 caracteres");
    dni = useGetNumber("Por favor, ingrese DNI: \t\tNo puede contener caracteres alfabeticos, solo numericos");
    pacienteService.createPaciente(nombre, apellido, dni);
  }

  private void createDoctor() {
    String nombre, apellido;
    int dni, matricula;
    showMessage("\t---------------------------------\nBienvenido a dar de alta a un nuevo doctor en el sistema!\n");
    nombre = dataInput("Por favor, ingrese Nombre: \t\tNo puede tener menos de 3 caracteres");
    apellido = dataInput("Por favor, ingrese Apellido: \t\tNo puede tener menos de 3 caracteres");
    dni = useGetNumber("Por favor, ingrese DNI: \t\tNo puede contener caracteres alfabeticos, solo numericos");
    matricula = useGetNumber(
        "Por favor, ingrese Matricula: \t\tNo puede contener caracteres alfabeticos, solo numericos");
    doctorService.createDoctor(nombre, apellido, dni, matricula);
  }

  private void gestionarTurno(Agenda agenda) {
    int input = -1;
    showMessage("\tSeleccione Dia para Gestionar turno\n");
    showMessage(agenda.toString());
    showMessage("\tSeleccione Dia para Gestionar turno\n");
    input = getNumber();
    Dia day = agenda.getDay(input);
    if (day != null) {
      gestionarTurnoMañanaTarde(day);
      gestionarTurnoDia(day);
    } else
      showMessage("No hay dias en la agenda con el dia ingresado");
  }

  private void gestionarTurnoDia(Dia day) {
    String m_t;
    String m = "m";
    String t = "t";
    String motivo;
    Paciente paciente;
    int hora_inicio = 0;
    int hora_fin = 0;
    int minuto_inicio = -1;
    int minuto_fin = -1;
    int id_paciente;
    showMessage("Ingrese 'm' para asignar turno-paciente en la mañana\n"
        + "Ingrese t para asignar turno-paciente en la tardde");
    m_t = dataInputChar("Solo el caracter 'm' para mañana o 't' para tarde");
    showMessage("ingreso. " + m_t);
    if (m_t.equals(m)) {
      showMessage("El turno mañana arranca: " + day.getTurno('m').getHoraMinutoInicio() + "\nIngrese Hora Inicio: ");
      hora_inicio = getNumber();
      if (hora_inicio < day.getTurno('m').getHoraInicio() || hora_inicio > day.getTurno('m').getHoraFin()) {
        showMessage("hora ingresada: " + hora_inicio + " no se encuentra en el rango "
            + day.getTurno('m').getHoraMinutoInicio() + " y " + day.getTurno('m').getHoraMinutoFin());
        return;
      }
      showMessage("Ingese los minutos: ");
      minuto_inicio = getNumber();
      if (minuto_inicio < 0 || minuto_inicio > 60) {
        showMessage("Los minutos no pueden ser menor que 0 y mayor a 59");
        return;
      }

      showMessage("Ingrese hora finalizacion del turno: ");
      hora_fin = getNumber();
      if (hora_fin < day.getTurno('m').getHoraInicio() || hora_fin > day.getTurno('m').getHoraFin()) {
        showMessage("La hora no esta en el rango del turno mañana " + day.getTurno('m').getHoraMinutoInicio() + " y "
            + day.getTurno('m').getHoraMinutoFin());
        return;
      }

      showMessage("Ingrese minuto finalizacion turno: ");
      minuto_fin = getNumber();
      if (minuto_fin < 0 || minuto_fin > 60) {
        showMessage("Los minutos estan en el rango 0 a 59, no hay minutos");
        return;
      }
      motivo = dataInput("Ingrese una breve descripcion del turno");
      showMessage("ingrese el id del paciente");
      pacienteService.showPacientes();
      showMessage("\t------------------------------------");
      id_paciente = getNumber();
      paciente = pacienteService.findById(id_paciente);
      if (paciente == null) {
        showMessage("No hay paciente con id: " + id_paciente);
        return;
      } else {
        day.getTurno('m').addTurno(new Turno(motivo, hora_inicio, minuto_inicio, hora_fin, minuto_fin, paciente));
      }
    } else if (m_t.equals(t)) {
      showMessage("Falta implementar turno tarde");
      showMessage("El turno tarde comienza: " + day.getTurno('t').getHoraMinutoInicio() + "\nIngrese Hora Inicio: ");
      hora_inicio = getNumber();
      if (hora_inicio < day.getTurno('t').getHoraInicio() || hora_inicio > day.getTurno('t').getHoraFin()) {
        showMessage("hora ingresada: " + hora_inicio + " no se encuentra en el rango "
            + day.getTurno('t').getHoraMinutoInicio() + " y " + day.getTurno('t').getHoraMinutoFin());
        return;
      }
      showMessage("Ingese los minutos: ");
      minuto_inicio = getNumber();
      if (minuto_inicio < 0 || minuto_inicio > 60) {
        showMessage("Los minutos no pueden ser menor que 0 y mayor a 59");
        return;
      }

      showMessage("Ingrese hora finalizacion del turno: ");
      hora_fin = getNumber();
      if (hora_fin < day.getTurno('t').getHoraInicio() || hora_fin > day.getTurno('t').getHoraFin()) {
        showMessage("La hora no esta en el rango del turno Tarde " + day.getTurno('t').getHoraMinutoInicio() + " y "
            + day.getTurno('t').getHoraMinutoFin());
        return;
      }

      showMessage("Ingrese minuto finalizacion turno: ");
      minuto_fin = getNumber();
      if (minuto_fin < 0 || minuto_fin > 60) {
        showMessage("Los minutos estan en el rango 0 a 59, no hay minutos");
        return;
      }
      motivo = dataInput("Ingrese una breve descripcion del turno");
      showMessage("ingrese el id del paciente");
      pacienteService.showPacientes();
      showMessage("\t------------------------------------");
      id_paciente = getNumber();
      paciente = pacienteService.findById(id_paciente);
      if (paciente == null) {
        showMessage("No hay paciente con id: " + id_paciente);
        return;
      } else {
        day.getTurno('t').addTurno(new Turno(motivo, hora_inicio, minuto_inicio, hora_fin, minuto_fin, paciente));
      }
    } else
      showMessage("No se ha ingresado ninguna opcion valida");
  }

  private void gestionarTurnoMañanaTarde(Dia dia) {
    DiaTurno dt = null;
    if (dia.getTurno('m') == null) {
      dt = createDiaTurnoMañana('m', dia.getNombre());
      if (dt == null)
        showMessage("No ha creado el turno mañana");
      else {
        dia.setTurno(dt);
        showMessage("Se ha creado el turno mañana, sin turnos-paciente asignados");
        showMessage(dia.toString());
      }
    }
    if (dia.getTurno('t') == null) {
      dt = null;
      dt = createDiaTurnoTarde('t', dia.getNombre());
      if (dt == null)
        showMessage(("No se ha creado el turno mañana"));
      else {
        dia.setTurno(dt);
        showMessage("Se ha creado el turno tarde, sin turnos-pacientes asignados");
        showMessage(dia.toString());
      }
    }
  }

  private DiaTurno createDiaTurnoMañana(char day, String dia) {
    showMessage("Ingrese Hora inicio Mañana: 6 - 12: ");
    hora_inicio = getNumber();
    while (hora_inicio < 6 || hora_inicio > 13) {
      showMessage("Respete el rango de hora entre 6 y 12, vuelva a ingresar la hora: ");
      hora_inicio = getNumber();
    }
    showMessage("Ingrese minutos Inicio en el rango de 0 a 59");
    minuto_inicio = getNumber();
    while (minuto_inicio < 0 || minuto_inicio > 60) {
      showMessage("Repsete el rango de los minutos entre 0 y 59");
      minuto_inicio = getNumber();
    }

    showMessage("Ingrese hora fin, no puede ser menor que hora inicio: " + hora_inicio + " ni mayor a 12");
    hora_fin = getNumber();
    while (hora_fin < hora_inicio || hora_fin > 13) {
      showMessage("Por favor, ingrese un horario correcto entre " + hora_inicio + " y las 13");
      hora_fin = getNumber();
    }

    showMessage("Ingrese minuto fin en el rango 0 a 59");
    minuto_fin = getNumber();
    while (minuto_fin < -1 || minuto_fin > 59) {
      showMessage("Por favor, ingrese los minutos en el rango de 0 - 59");
      minuto_fin = getNumber();
    }

    return diaTurnoService.createDiaTurno('m', "mañana", hora_inicio, minuto_inicio, hora_fin, minuto_fin);
  }

  private DiaTurno createDiaTurnoTarde(char day, String dia) {
    showMessage("Ingrese Hora inicio Tarde: 13 a 19: ");
    hora_inicio = getNumber();
    while (hora_inicio < 13 || hora_inicio > 19) {
      showMessage("Respete el rango de hora entre 13 y 19, vuelva a ingresar la hora: ");
      hora_inicio = getNumber();
    }
    showMessage("Ingrese minutos Inicio en el rango de 0 a 59");
    minuto_inicio = getNumber();
    while (minuto_inicio < 0 || minuto_inicio > 60) {
      showMessage("Repsete el rango de los minutos entre 0 y 59");
      minuto_inicio = getNumber();
    }

    showMessage("Ingrese hora fin, no puede ser menor que hora inicio: " + hora_inicio + " ni mayor a 19");
    hora_fin = getNumber();
    while (hora_fin < hora_inicio || hora_fin > 19) {
      showMessage("Por favor, ingrese un horario correcto entre " + hora_inicio + 1 + " y las 13");
      hora_fin = getNumber();
    }

    showMessage("Ingrese minuto fin en el rango 0 a 59");
    minuto_fin = getNumber();
    while (minuto_fin < -1 || minuto_fin > 59) {
      showMessage("Por favor, ingrese los minutos en el rango de 0 - 59");
      minuto_fin = getNumber();
    }

    return diaTurnoService.createDiaTurno('t', "tarde", hora_inicio, minuto_inicio, hora_fin, minuto_fin);
  }

  private void showMenu() {
    this.menu = "\n\t-----------------------------------------\n" + "\t* Listar Doctores: \t\t\t1\n"
        + "\t* Listar Pacientes: \t\t\t2\n" + "\t* Listar Agenda de Doctor: \t\t3\n"
        + "\t* Crear Turno Tarde-Mañana Agenda Doctor\t4\n" + "\t* Crear Doctor\t\t\t\t5\n"
        + "\t* Crear Paciente\t\t\t6\n" + "\t\t*** Salir ** \t0";
    System.out.println(this.menu);
  }

  private String dataInput(String message) {
    String string_in = "";
    do {
      System.out.println(message);
      string_in = getString();
    } while (string_in.length() <= 3);
    return string_in;
  }

  private String dataInputChar(String message) {
    String string_in = "";
    do {
      System.out.println(message);
      string_in = getChar();
    } while (string_in.length() != 1);
    return string_in;
  }

  private void showMessage(String message) {
    System.out.println(message);
  }

  // private String stringInput(String type) {
  // do {
  // System.out.println("Por favor, ingrese " + type);
  // name_aux = getString();
  // } while (name_aux.length() <= 3);
  // return name_aux;
  // }

  private int getNumber() {
    scanner = new Scanner(System.in);
    int number;
    try {
      number = scanner.nextInt();
      return number;
    } catch (InputMismatchException e) {
      System.out.println("\nPor favor, ingrese caracter numerico\n");
      return -1;
    }
  }

  private int useGetNumber(String message) {
    System.out.println(message);
    int num = -1;
    do {
      num = getNumber();
    } while (num == -1);
    return num;
  }

  private String getString() {
    scanner = new Scanner(System.in);
    try {
      input_string = scanner.nextLine();
      if ((int) (input_string.length()) < 3) {
        System.out.println("\n*** La longitud de palabra no puede ser menor a tres caracteres");
        return "";
      }
      return input_string;
    } catch (InputMismatchException e) {
      System.out.println("\n*** Por favor, ingrese una cadena\n");
      return "";
    }
  }

  private String getChar() {
    scanner = new Scanner(System.in);
    try {
      input_string = scanner.nextLine();
      if ((int) (input_string.length()) > 1) {
        System.out.println("\n Ingrese solo un caracter");
        return "";
      }
      return input_string;
    } catch (InputMismatchException e) {
      System.out.println("\tSolo un caracter\n");
      return "";
    }
  }

  /*
   * 
   * System.out.println("\tLos turno se asignan para a agenda de un doctor.");
   * message = "Para generar Agenda Mañana, ingrese 1\n" +
   * "Para generar Agenda Tarde, ingrese 2"; showMessage(message); opcion =
   * getNumber(); if (opcion == 1) { hora_inicio = 0; minuto_inicio = -1; hora_fin
   * = 0; minuto_fin = -1; System.out.
   * println("Ingrese hora y minuto por separado cuando se le solicite\n"); do {
   * if( hora_inicio < 6 || hora_inicio > 13){ System.out.
   * println("Por favor, ingrese un hora inico, debe ser mayor a 6 AM. Ej: 8 ");
   * hora_inicio = getNumber(); } else if(minuto_inicio < 0 || minuto_inicio > 60
   * ){ System.out.println("Ingrese minutos de inicio, rango: 0 - 59");
   * minuto_inicio = getNumber(); } else break; } while (true); do { if( hora_fin
   * > 13 || hora_fin < hora_inicio){ System.out.
   * println("Por favor, ingrese hora fin, debe ser mayor a hora inicio y menor a 13. Ej: 12 "
   * ); hora_fin = getNumber(); } else if(minuto_fin < 0 || minuto_fin > 60 ){
   * System.out.println("Ingrese minutos de fin, rango: 0 - 59"); minuto_fin =
   * getNumber(); } else break; } while (true); System.out.println(hora_inicio +
   * ":" + minuto_inicio + "    " + hora_fin + ":" + minuto_fin);
   * 
   * 
   * } else if (opcion == 2) {
   * 
   * } else {
   * System.out.println("\t*** No se ha ingresado una opcion valida.\n\n"); }
   * 
   */
}