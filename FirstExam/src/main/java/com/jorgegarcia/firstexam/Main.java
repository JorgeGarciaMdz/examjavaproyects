/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jorgegarcia.firstexam;

/**
 *
 * @author jorge
 * mvn exec:java -Dexec.mainClass="com.jorgegarcia.firstexam.Main"
 */
public class Main {
    public static void main(String [] args){
        System.out.println("\n");
        Fachada fachada = new Fachada();
        fachada.init();
    }
}
