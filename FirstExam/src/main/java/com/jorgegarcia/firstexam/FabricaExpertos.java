/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jorgegarcia.firstexam;
import com.jorgegarcia.firstexam.Escuela.ExpertoEscuela;
import com.jorgegarcia.firstexam.BusquedaCliente.ExpertoBusqueda;
import com.jorgegarcia.firstexam.Agenda.ExpertoAgenda;

/**
 *
 * @author jorge
 */
public class FabricaExpertos {
    private static Experto experto;           
    
    public static Experto getExperto(int tipoExperto){
        switch( tipoExperto ){
            case 1: 
                experto = new ExpertoEscuela();
                break;
            case 2:
                experto = new ExpertoBusqueda();
                break;
            case 3:
                experto = new ExpertoAgenda();
                break;
            default:
                experto = null;
        }
        return experto;
    } 
}
