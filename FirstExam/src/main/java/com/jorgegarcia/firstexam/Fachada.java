/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jorgegarcia.firstexam;
import java.util.Scanner;
import java.util.InputMismatchException;

/**
 *
 * @author jorge
 */
public class Fachada {
    private Experto experto;
    private int opcion;
    // private int inputNumber;
    private String menu = "\t   Experto \t\tOpcion\n" +
                           "\t   ----------------- \t------ \n" +
                            "\t** Escuela \t\t  1\n" + "\t** Busqueda Cliente \t  2\n" +
                            "\t** Agenda \t\t  3\n" + "\t***** Salir *****\t  0\n";
    Scanner scanner;

    // public Fachada(){
    //     scanner = new Scanner(System.in);
    //     inputNumber = -1;
    // }

    public void init(){
        System.out.println(menu);
        while(true){
            opcion = getNumber();
            if( opcion == 0 ){
                break;
            } else if( opcion >= 0 && opcion <= 3 ){
                experto = FabricaExpertos.getExperto(opcion);
                try {
                    experto.start();
                } catch (NullPointerException e) {
                    // TODO: handle exception
                }
                System.out.println("\n" + menu);
            } else {
                System.out.println(menu);
                continue;
            }
        }
        System.out.println("Exit void init");
    }

    private int getNumber(){
        scanner = new Scanner(System.in);
        int number;
        try{
          number = scanner.nextInt();
          return number;
        } catch(InputMismatchException e){
          System.out.println("\nPlease, input a number\n");
          return -1;
        }
    }
}
