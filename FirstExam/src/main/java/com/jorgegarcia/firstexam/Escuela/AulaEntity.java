package com.jorgegarcia.firstexam.Escuela;

public class AulaEntity{
  private int id;
  private String nombre;
  private int capacidad;
  private int ocupado;

  public AulaEntity(int id, String nombre, int capacidad){
    this.id = id;
    this.nombre = nombre;
    this.capacidad = capacidad;
    this.ocupado = 0;
  }

  public int getDisponible(){ return (capacidad - ocupado); }
  public void setDisponible(int enUso){
    ocupado += enUso;
  }

  public void setResizeDisponible( int add){ this.ocupado -= add; }

  public int getOcupado(){ return this.ocupado; }

  public String getNombre(){ return this.nombre; }

  public int getCapacidad(){ return this.capacidad; }

  public int getId(){ return this.id; }
}