package com.jorgegarcia.firstexam.Escuela;
import java.util.Scanner;
import java.util.InputMismatchException;

public class AulaController{
  private AulaService aulaService;
  private String message = "Por favor, ingrese la cantidad de bancos a asignar, la cantidad ingresada no\n"+
        "debe superar la capacidad del aula con mayor capacidad, ingrese 0 para salir. "; 
  private int bancos;
  private Scanner scanner;
  public AulaController(){
    aulaService = new AulaService();
  }

  public void menuBancos(){
    System.out.println("\n\n\tBienvenido a Asignacion de Bancos Dinamico!\n");
    System.out.println( aulaService.stringBancos() + "\n");
    System.out.println(message);

    while(true){
      bancos = getNumber();
      if( bancos == 0 ){
        System.out.println("\t***** Good Bye! *****");
        break;
      } else if( bancos > 0 && bancos <= 40 ){
        aulaService.setBancos(bancos);
        System.out.println( aulaService.stringBancos() + "\n");
        System.out.println("Para salir de Gestion de Bancos, ingrese 0.\n");
        System.out.println("De otro modo, ingrese cantidad de bancos: ");
        continue;
      } else 
          System.out.println("Por favor, ingrese un numero valido, entre 1 y 40, 0 para salir.");
    }
    
  }

  private int getNumber(){
    scanner = new Scanner(System.in);
    int number;
    try{
      number = scanner.nextInt();
      return number;
    } catch(InputMismatchException e){
      System.out.println("\nPlease, input a number\n");
      return -1;
    }
  }
}