package com.jorgegarcia.firstexam.Escuela;
import java.lang.Math;

public class AulaService{
  private AulaEntity arrayAula[];
  private int sub_banco_1, sub_banco_2, sub_banco_3, dif_bancos;

  public AulaService(){
    arrayAula = new AulaEntity[3];
    arrayAula[0] = new AulaEntity(1, "Azul", 40);
    arrayAula[1] = new AulaEntity(2, "Verde", 35);
    arrayAula[2] = new AulaEntity(3, "Amarillo", 30);
  }

  public boolean setBancos(int bancos){
    System.out.println("\tBancos ingresados a ser asignados: " + bancos);
    sub_banco_1 = (int) (bancos * 0.4);
    sub_banco_2 = (int) (bancos * 0.3);
    sub_banco_3 = (int) (bancos * 0.3);
    // System.out.println("b1: " + sub_banco_1 + " b2: " + sub_banco_2 + " sb3: " + sub_banco_3);
    try {
      dif_bancos = bancos % (sub_banco_1 + sub_banco_2 + sub_banco_3 );
    } catch (ArithmeticException e) {
      dif_bancos = 0;
    }
    // System.out.println("dif_bancos: " + dif_bancos);

    if((arrayAula[0].getDisponible() + arrayAula[1].getDisponible() + arrayAula[2].getDisponible() ) == 0 ){
      System.out.println("\n\t**** Ya no hay cupos disponibles. ****\n");
      return true;
    }

    if( bancos <= 3) {
      //System.out.println("bancos =< 3");
      if( arrayAula[2].getDisponible() >= bancos ){
        arrayAula[2].setDisponible(bancos);
        return true;
      } else if( arrayAula[1].getDisponible() >= bancos ){
        arrayAula[1].setDisponible(bancos);
        return true;
      } else if( arrayAula[0].getDisponible() >= bancos ){
        arrayAula[0].setDisponible(bancos);
        return true;
      }
    } else if( 
      (arrayAula[0].getDisponible() + arrayAula[1].getDisponible() + arrayAula[2].getDisponible() ) > bancos
     ) {
      for( int i = 0; i < arrayAula.length; i++){
        switch (i) {
          case 0:{
            if( ( dif_bancos + sub_banco_1 ) <= arrayAula[i].getDisponible() ){
              // System.out.println("case 0, in if");
              arrayAula[i].setDisponible(dif_bancos + sub_banco_1);
              arrayAula[i+1].setDisponible(sub_banco_2);
              arrayAula[i+2].setDisponible(sub_banco_3);
              if(isNegative())
                reDistribuir();
              return true;
            }
            break;
          }
          
          case 1: {
            if( ( dif_bancos + sub_banco_2 ) <= arrayAula[i].getDisponible() ){
            // System.out.println("case 1, in if");
              arrayAula[i-1].setDisponible(sub_banco_1);
              arrayAula[i].setDisponible(dif_bancos + sub_banco_2);
              arrayAula[i+1].setDisponible(sub_banco_3);
              if(isNegative())
                reDistribuir();
              return true;
            }
            break;
          }

          case 2: {
            if( (dif_bancos + sub_banco_3 ) <= arrayAula[i].getDisponible() ){
            // System.out.println("case 2, in if");
              arrayAula[i-2].setDisponible(sub_banco_1);
              arrayAula[i-1].setDisponible(sub_banco_2);
              arrayAula[i].setDisponible(dif_bancos + sub_banco_3);
              if(isNegative())
                reDistribuir();
              return true;
            }
            break;
          }
          default:
            return false;
        }
      }
    } else if(
      bancos == arrayAula[0].getDisponible() || bancos == arrayAula[1].getDisponible() 
      || bancos == arrayAula[2].getDisponible() 
    ) {
      for (int i = 0; i < arrayAula.length; i++){
        if( arrayAula[i].getDisponible() >= bancos){
          arrayAula[i].setDisponible(bancos);
          return true;
        }
      }
    }
    return false;
  } 

  private void reDistribuir(){
    int aux_neg;
    // System.out.println("--> re distribuir");
    int index_mayor = 0, index_menor = 0;
    // averiguo el indice del elemento que tiene valor negativo
    // System.out.println("\n" + stringBancos() + "\n");
    for(int i = 0; i < arrayAula.length ; i++)
      if( arrayAula[i].getDisponible() < 0 ){
        index_menor = i;
        // System.out.println("index_menor: " + index_menor);
      }
    
    //averiguo el indice del que tiene mayor capacidad
    for(int i = 0; i < arrayAula.length -1 ; i++)
      if( arrayAula[index_mayor].getDisponible() < arrayAula[i+1].getDisponible() ){
        index_mayor = i + 1;
        // System.out.println("index_mayor: " + index_mayor);
      }

    if( ( arrayAula[index_mayor].getDisponible() - arrayAula[index_menor].getDisponible()) >=0 ){
      arrayAula[index_mayor].setDisponible( Math.abs(arrayAula[index_menor].getDisponible()) );
      aux_neg = Math.abs(arrayAula[index_menor].getDisponible());
      arrayAula[index_menor].setResizeDisponible(aux_neg);
    }

  }

  public boolean isNegative(){
    // System.out.println("--> is negative");
    for(int i = 0; i < arrayAula.length; i++)
      if(arrayAula[i].getDisponible() < 0 )
        return true;
    return false;
  }

  public String stringBancos(){
    return "\t Estado de Capacidad de las Aulas\n" + 
            arrayAula[0].getNombre() + "\t\tCapacidad: " + arrayAula[0].getCapacidad() +
            "\tDisponibilidad: " + arrayAula[0].getDisponible() + "\tOcupados: " + arrayAula[0].getOcupado()+
              "\n" +

            arrayAula[1].getNombre() + "\t\tCapacidad: " + arrayAula[1].getCapacidad() +
            "\tDisponibilidad: " + arrayAula[1].getDisponible() + "\tOcupados: " + arrayAula[1].getOcupado()+
            "\n" +

            arrayAula[2].getNombre() + "\tCapacidad: " + arrayAula[2].getCapacidad() +
            "\tDisponibilidad: " + arrayAula[2].getDisponible() + "\tOcupados: " + arrayAula[2].getOcupado()+
            "\n" +

            "Total Bancos Disponibles: " + (arrayAula[0].getDisponible() + arrayAula[1].getDisponible() + 
                                    arrayAula[2].getDisponible());
  }
}